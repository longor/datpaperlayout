﻿DatPaperLayout
==============

DatPaperLayour, is a small library that helps with building layouts
made out of rectangles stored in a tree-like manner, similar to HTML.

It supports different 'Layout Managers',
each of them having their own way of laying out the rectangles.
There is also a 'NULL'-Layout, where you can position the rectangles
directly by specifiyng their position and size.

This is a standalone library, meaning that it does not have any
dependencies on any code outside of the Java-Runtime-Library and itself.

  [Imgur](http://i.imgur.com/aNoEDs9.png)

----

The basic usage is as follows:

    // Basic Usage Example
    int screenWidth = ...;
    int screenHeight = ...;
    
    // Create a new Paper-Interpreter
	PaperInterpreter interpreter = new PaperInterpreter();
	ContainerNode root = interpreter.getRoot();
	root.addAttribute(LayoutAttribute.compassLayout());
    
    // Create a Rectangle with a size of 64x64 units at the center of the screen.
	{
		RectangleNode node = new RectangleNode(root);
		node.setPreferredBounds(new RectangleF(64, 64));
		node.addAttribute(CompassAligmentAttribute.center("vertical-align"));
		node.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
	}
    
    // Create a Container with half the screen-height and half the screen-width,
    // place it at the center of the screen,
    // and put 4*4 rectangles layed out in a grid inside it.
	{
		// creation
		ContainerNode cnode = new ContainerNode(root);
		cnode.setPreferredBounds(new RectangleF(1, 1));
		
		// size definition
		cnode.addAttribute(SizeAttribute.createPercentage("width", 50));
		cnode.addAttribute(SizeAttribute.createPercentage("height", 50));
		
		// position definition
		cnode.addAttribute(CompassAligmentAttribute.center("vertical-align"));
		cnode.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
		
		// grid layout definition
		cnode.addAttribute(LayoutAttribute.gridLayout());
		cnode.addAttribute(NumberAttribute.createUnit("cells-x", 4));
		cnode.addAttribute(NumberAttribute.createUnit("cells-y", 4));
		
		// create the 4*4 rectangles inside the big rectangle
		int f = 4 * 4;
		for(int i = 0; i < f; i++)
		{
			RectangleNode node = new RectangleNode(cnode);
			node.setPreferredBounds(new RectangleF(16, 16));
		}	
	}
	
	// Let the PaperInterpreter layout the whole thing...
	interpreter.doLayout(SCR_WIDTH, SCR_HEIGHT);
	
	// Done!
	// At this point, all ContainerNode's and RectangleNode's in the tree
	// have their final bounds stored in their finalizedBounds field.


License
=======
I do NOT know a lot about licenses, neither do I really care,
so I will just write down the 'usage rules' here:

 * You are allowed to use this library.
 * You are allowed to use this library in a commercial product.
 * You are not allowed to remove my name from the library.
 * I am not responsible for any damage this library might cause in any possible way.

