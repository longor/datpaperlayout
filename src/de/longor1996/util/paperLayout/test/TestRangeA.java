package de.longor1996.util.paperLayout.test;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;

import org.junit.Test;

import de.longor1996.util.math.geom2D.RectangleF;
import de.longor1996.util.paperLayout.ContainerNode;
import de.longor1996.util.paperLayout.PaperInterpreter;
import de.longor1996.util.paperLayout.RectangleNode;
import de.longor1996.util.paperLayout.attributes.CompassAligmentAttribute;
import de.longor1996.util.paperLayout.attributes.LayoutAttribute;
import de.longor1996.util.paperLayout.attributes.NumberAttribute;
import de.longor1996.util.paperLayout.attributes.PositionAttribute;
import de.longor1996.util.paperLayout.attributes.SizeAttribute;
import de.longor1996.util.paperLayout.attributes.StringAttribute;

public class TestRangeA
{
	public static BufferedImage faceImage;
	
	@Test
	public void test1()
	{
	    // Basic Usage Example
	    int screenWidth = 800;
	    int screenHeight = 500;
	    
	    // Create a new Paper-Interpreter
		PaperInterpreter interpreter = new PaperInterpreter();
		ContainerNode root = interpreter.getRoot();
		root.addAttribute(LayoutAttribute.compassLayout());
		interpreter.setDebug(true);
	    
	    // Create a Rectangle with a size of 64x64 units at the center of the screen.
		{
			RectangleNode node = new RectangleNode(root);
			node.setPreferredBounds(new RectangleF(64, 64));
			node.addAttribute(CompassAligmentAttribute.north("vertical-align"));
			node.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
		}
	    
	    // Create a Container with half the screen-height and half the screen-width,
	    // place it at the center of the screen,
	    // and put 4*4 rectangles layed out in a grid inside it.
		{
			// creation
			ContainerNode cnode = new ContainerNode(root);
			cnode.setPreferredBounds(new RectangleF(1, 1));
			
			// size definition
			cnode.addAttribute(SizeAttribute.createPercentage("width", 50));
			cnode.addAttribute(SizeAttribute.createPercentage("height", 50));
			
			// position definition
			cnode.addAttribute(CompassAligmentAttribute.center("vertical-align"));
			cnode.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
			
			// grid layout definition
			cnode.addAttribute(LayoutAttribute.gridLayout());
			cnode.addAttribute(NumberAttribute.createUnit("cells-x", 4));
			cnode.addAttribute(NumberAttribute.createUnit("cells-y", 4));
			
			// create the 4*4 rectangles inside the big rectangle
			int f = 4 * 4;
			for(int i = 0; i < f; i++)
			{
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(16, 16));
			}
		}
		
		// Let the PaperInterpreter layout the whole thing...
		interpreter.doLayout(screenWidth, screenHeight);
		
		// Done!
		// At this point, all ContainerNode's and RectangleNode's in the tree
		// have their final bounds stored in their finalizedBounds field.
		
		this.buildDebugImage(screenWidth, screenHeight, root, "result_test1.png");
		
	}
	
	@Test
	public void test2()
	{
		final int screenWidth = 1920;
		final int screenHeight = 1080;
		
		PaperInterpreter interpreter = new PaperInterpreter();
		ContainerNode root = interpreter.getRoot();
		root.addAttribute(LayoutAttribute.compassLayout());
		interpreter.setDebug(true);
		
		{
			ContainerNode cnode = new ContainerNode(root);
			cnode.setPreferredBounds(new RectangleF(64, 96));
			cnode.addAttribute(LayoutAttribute.compassLayout());
			cnode.addAttribute(CompassAligmentAttribute.north("vertical-align"));
			cnode.addAttribute(CompassAligmentAttribute.west("horizontal-align"));
			
			{
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(16, 64));
				node.addAttribute(CompassAligmentAttribute.north("vertical-align"));
				node.addAttribute(CompassAligmentAttribute.west("horizontal-align"));
			}
			
			{
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(24, 24));
				node.addAttribute(CompassAligmentAttribute.center("vertical-align"));
				node.addAttribute(CompassAligmentAttribute.east("horizontal-align"));
			}
			
			{
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(8, 8));
				node.addAttribute(CompassAligmentAttribute.south("vertical-align"));
				node.addAttribute(CompassAligmentAttribute.west("horizontal-align"));
			}
			
			{
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(8, 8));
				node.addAttribute(CompassAligmentAttribute.center("vertical-align"));
				node.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
			}
		}
		
		{
			ContainerNode cnode = new ContainerNode(root);
			cnode.setPreferredBounds(new RectangleF(64, 96));
			cnode.addAttribute(LayoutAttribute.compassLayout());
			cnode.addAttribute(CompassAligmentAttribute.south("vertical-align"));
			cnode.addAttribute(CompassAligmentAttribute.west("horizontal-align"));
			
			{
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(16, 64));
				node.addAttribute(CompassAligmentAttribute.north("vertical-align"));
				node.addAttribute(CompassAligmentAttribute.west("horizontal-align"));
			}
			
			{
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(24, 24));
				node.addAttribute(CompassAligmentAttribute.center("vertical-align"));
				node.addAttribute(CompassAligmentAttribute.east("horizontal-align"));
				node.addAttribute(SizeAttribute.createPercentage("height", 50));
			}
		}
		
		{
			RectangleNode node = new RectangleNode(root);
			node.setPreferredBounds(new RectangleF(48, 96));
			node.addAttribute(CompassAligmentAttribute.center("vertical-align"));
			node.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
			node.addAttribute(SizeAttribute.createPercentage("width", 75));
			node.addAttribute(SizeAttribute.createPercentage("height", 50));
		}
		
		{
			ContainerNode cnode = new ContainerNode(root);
			cnode.setPreferredBounds(new RectangleF(256, 256));
			cnode.addAttribute(CompassAligmentAttribute.center("vertical-align"));
			cnode.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
			cnode.addAttribute(StringAttribute.create("id", "Big Box"));
			cnode.addAttribute(LayoutAttribute.gridLayout());
			cnode.addAttribute(NumberAttribute.createUnit("cells-x", 4));
			cnode.addAttribute(NumberAttribute.createUnit("cells-y", 4));
			
			int f = 4 * 4;
			for(int i = 0; i < f; i++)
			{
				if(i == 0)
				{
					ContainerNode bnode = new ContainerNode(cnode);
					bnode.setPreferredBounds(new RectangleF(16, 16));
					bnode.addAttribute(LayoutAttribute.gridLayout());
					bnode.addAttribute(NumberAttribute.createUnit("cells-x", 4));
					bnode.addAttribute(NumberAttribute.createUnit("cells-y", 4));
					
					int f2 = 4 * 4;
					for(int i2 = 0; i2 < f2; i2++)
					{
						RectangleNode node = new RectangleNode(bnode);
						node.setPreferredBounds(new RectangleF(16, 16));
					}
					
					continue;
				}
				
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(16, 16));
			}
			
		}
		
		{
			RectangleNode node = new RectangleNode(root);
			node.setPreferredBounds(new RectangleF(16, 24));
			node.addAttribute(CompassAligmentAttribute.south("vertical-align"));
			node.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
			node.addAttribute(SizeAttribute.createPercentage("width", 50));
		}
		
		{
			ContainerNode cnode = new ContainerNode(root);
			cnode.setPreferredBounds(new RectangleF(64, 64));
			cnode.addAttribute(CompassAligmentAttribute.north("vertical-align"));
			cnode.addAttribute(CompassAligmentAttribute.east("horizontal-align"));
			cnode.addAttribute(SizeAttribute.createPercentage("width", 50));
			cnode.addAttribute(StringAttribute.create("id", "Big Box"));
			cnode.addAttribute(LayoutAttribute.gridLayout());
			cnode.addAttribute(NumberAttribute.createUnit("cells-x", 16));
			cnode.addAttribute(NumberAttribute.createUnit("cells-y", 3));
			
			int f = 16 * 3;
			for(int i = 0; i < f; i++)
			{
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(16, 16));
				node.addAttribute(CompassAligmentAttribute.center("vertical-align"));
				node.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
			}
		}
		
		{
			RectangleNode node = new RectangleNode(root);
			node.setPreferredBounds(new RectangleF(32, 32));
			node.addAttribute(CompassAligmentAttribute.south("vertical-align"));
			node.addAttribute(CompassAligmentAttribute.east("horizontal-align"));
			node.addAttribute(PositionAttribute.createPercentage("offset-x", -5));
			node.addAttribute(PositionAttribute.createPercentage("offset-y", -5));
		}
		
		interpreter.doLayout(screenWidth, screenHeight);
		
		this.buildDebugImage(screenWidth, screenHeight, root, "result_test2.png");
	}
	
	@Test
	public void test3()
	{
	    // Basic Usage Example
	    int screenWidth = 800;
	    int screenHeight = 500;
	    
	    // Create a new Paper-Interpreter
		PaperInterpreter interpreter = new PaperInterpreter();
		ContainerNode root = interpreter.getRoot();
		root.addAttribute(LayoutAttribute.compassLayout());
		interpreter.setDebug(true);
	    
		// LEFT SIDE
		{
			ContainerNode cnode = new ContainerNode(root);
			cnode.setPreferredBounds(new RectangleF(1, 96));
			cnode.addAttribute(CompassAligmentAttribute.north("vertical-align"));
			cnode.addAttribute(CompassAligmentAttribute.west("horizontal-align"));
			cnode.addAttribute(SizeAttribute.createPercentage("width", 20));
			cnode.addAttribute(SizeAttribute.createPercentage("height", 75));
			cnode.addAttribute(LayoutAttribute.flowLayout());
			cnode.addAttribute(StringAttribute.create("direction", "vertical"));
			cnode.addAttribute(CompassAligmentAttribute.east("alignment"));
			
			{
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(32, 32));
			}
			
			Random random = new Random(420024);
			int[] sizes = new int[]{16, 16, 16, 32, 32, 32, 42, 42, 64, 64};
			
			for(int i = 0; i < 7; i++)
			{
				int size = sizes[random.nextInt(sizes.length)];
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(size, size));
			}
		}
		
		// RIGHT SIDE
		{
			ContainerNode cnode = new ContainerNode(root);
			cnode.setPreferredBounds(new RectangleF(1, 96));
			cnode.addAttribute(CompassAligmentAttribute.north("vertical-align"));
			cnode.addAttribute(CompassAligmentAttribute.east("horizontal-align"));
			cnode.addAttribute(SizeAttribute.createPercentage("width", 20));
			cnode.addAttribute(SizeAttribute.createPercentage("height", 75));
			cnode.addAttribute(LayoutAttribute.flowLayout());
			cnode.addAttribute(StringAttribute.create("direction", "vertical"));
			cnode.addAttribute(CompassAligmentAttribute.west("alignment"));
			
			{
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(32, 32));
			}
			
			Random random = new Random(428824);
			int[] sizes = new int[]{16, 16, 16, 32, 32, 32, 42, 42, 64, 64};
			
			for(int i = 0; i < 9; i++)
			{
				int size = sizes[random.nextInt(sizes.length)];
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(size, size));
			}
		}
		
		// BOTTOM SIDE
		{
			ContainerNode cnode = new ContainerNode(root);
			cnode.setPreferredBounds(new RectangleF(1, 96));
			cnode.addAttribute(CompassAligmentAttribute.south("vertical-align"));
			cnode.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
			cnode.addAttribute(SizeAttribute.createPercentage("width", 100));
			cnode.addAttribute(SizeAttribute.createPercentage("height", 25));
			cnode.addAttribute(LayoutAttribute.flowLayout());
			cnode.addAttribute(StringAttribute.create("direction", "horizontal"));
			cnode.addAttribute(CompassAligmentAttribute.center("alignment"));
			
			{
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(32, 32));
			}
			
			Random random = new Random(424242);
			int[] sizes = new int[]{16, 16, 16, 32, 32, 32, 42, 42, 64, 64, 64, 96};
			
			for(int i = 0; i < 18; i++)
			{
				int size = sizes[random.nextInt(sizes.length)];
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(size, size));
			}
		}
		
		// TOP SIDE
		{
			ContainerNode cnode = new ContainerNode(root);
			cnode.setPreferredBounds(new RectangleF(1, 96));
			cnode.addAttribute(CompassAligmentAttribute.north("vertical-align"));
			cnode.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
			cnode.addAttribute(SizeAttribute.createPercentage("width", 60));
			cnode.addAttribute(SizeAttribute.createPercentage("height", 25));
			cnode.addAttribute(LayoutAttribute.flowLayout());
			cnode.addAttribute(StringAttribute.create("direction", "horizontal"));
			cnode.addAttribute(CompassAligmentAttribute.south("alignment"));
			
			{
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(32, 32));
			}
			
			Random random = new Random(423342);
			int[] sizes = new int[]{16, 16, 16, 32, 32, 32, 42, 42, 64, 64, 64, 96};
			
			for(int i = 0; i < 8; i++)
			{
				int size = sizes[random.nextInt(sizes.length)];
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(size, size));
			}
		}
		
		interpreter.doLayout(screenWidth, screenHeight);
		
		this.buildDebugImage(screenWidth, screenHeight, root, "result_test3.png");
		
	}
	
	@Test
	public void test4()
	{
	    // Basic Usage Example
	    int screenWidth = 800;
	    int screenHeight = 500;
	    
	    // Create a new Paper-Interpreter
		PaperInterpreter interpreter = new PaperInterpreter();
		ContainerNode root = interpreter.getRoot();
		root.addAttribute(LayoutAttribute.compassLayout());
		interpreter.setDebug(true);
		
		{
			// creation
			ContainerNode cnode = new ContainerNode(root);
			cnode.setPreferredBounds(new RectangleF(1, 1));
			
			// size definition
			cnode.addAttribute(SizeAttribute.createPercentage("width", 100));
			cnode.addAttribute(SizeAttribute.createPercentage("height", 100));
			
			// position definition
			cnode.addAttribute(CompassAligmentAttribute.center("vertical-align"));
			cnode.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
			
			// splitpane layout definition
			cnode.addAttribute(LayoutAttribute.splitpaneLayout());
			cnode.addAttribute(StringAttribute.create("direction", "vertical"));
			cnode.addAttribute(SizeAttribute.createPercentage("splitpos", 20F));
			
			{
				RectangleNode node = new RectangleNode(cnode);
				node.setPreferredBounds(new RectangleF(16, 16));
			}
			
			{
				// creation
				ContainerNode ccnode = new ContainerNode(cnode);
				ccnode.setPreferredBounds(new RectangleF(1, 1));
				
				// position definition
				ccnode.addAttribute(CompassAligmentAttribute.center("vertical-align"));
				ccnode.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
				
				// splitpane layout definition
				ccnode.addAttribute(LayoutAttribute.splitpaneLayout());
				ccnode.addAttribute(StringAttribute.create("direction", "horizontal"));
				ccnode.addAttribute(SizeAttribute.createPercentage("splitpos", 10F));
				
				{
					RectangleNode node = new RectangleNode(ccnode);
					node.setPreferredBounds(new RectangleF(16, 16));
				}
				
				{
					RectangleNode node = new RectangleNode(ccnode);
					node.setPreferredBounds(new RectangleF(16, 16));
					node.addAttribute(SizeAttribute.createPercentage("padding-x", 10));
					node.addAttribute(SizeAttribute.createPercentage("padding-y", 10));
				}
			}
		}
		
		interpreter.doLayout(screenWidth, screenHeight);
		
		this.buildDebugImage(screenWidth, screenHeight, root, "result_test4.png");
		
	}
	
	@Test
	public void test5()
	{
	    // Basic Usage Example
	    int screenWidth = 800;
	    int screenHeight = 500;
	    
	    // Create a new Paper-Interpreter
		PaperInterpreter interpreter = new PaperInterpreter();
		ContainerNode root = interpreter.getRoot();
		root.addAttribute(LayoutAttribute.compassLayout());
		interpreter.setDebug(true);
		
		root.addAttribute(LayoutAttribute.compassLayout());
		{
			RectangleNode node = new RectangleNode(root);
			node.addAttribute(SizeAttribute.createPercentage("width", 100));
			node.addAttribute(SizeAttribute.createPercentage("height", 100));
			node.addAttribute(SizeAttribute.createPercentage("padding-north", 5));
			node.addAttribute(SizeAttribute.createPercentage("padding-south", 70));
			node.addAttribute(SizeAttribute.createPercentage("padding-west", 20));
			node.addAttribute(SizeAttribute.createPercentage("padding-east", 20));
		}
		{
			RectangleNode node = new RectangleNode(root);
			node.addAttribute(SizeAttribute.createPercentage("width", 100));
			node.addAttribute(SizeAttribute.createPercentage("height", 100));
			
			node.addAttribute(SizeAttribute.createPercentage("padding-north", 30));
			node.addAttribute(SizeAttribute.createPercentage("padding-west", 20));
			node.addAttribute(SizeAttribute.createPercentage("padding-east", 20));
		}
		
		interpreter.doLayout(screenWidth, screenHeight);
		
		this.buildDebugImage(screenWidth, screenHeight, root, "result_test5.png");
	}
	
	
	
	
	@Test
	public void bigtest()
	{
	    // Basic Usage Example
	    int screenWidth = 800;
	    int screenHeight = 500;
	    
	    // Create a new Paper-Interpreter
		PaperInterpreter interpreter = new PaperInterpreter();
		ContainerNode root = interpreter.getRoot();
		root.addAttribute(LayoutAttribute.compassLayout());
		interpreter.setDebug(true);
		
		this.bigtest_do(root);
		
		interpreter.doLayout(screenWidth, screenHeight);
		this.buildDebugImage(screenWidth, screenHeight, root, "big-test.png");
	}
	
	private void bigtest_do(ContainerNode root)
	{
		root.addAttribute(LayoutAttribute.compassLayout());
		
		// Top Bar
		{
			ContainerNode topbar = new ContainerNode(root);
			
			// position
			topbar.addAttribute(CompassAligmentAttribute.north("vertical-align"));
			topbar.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
			
			// size
			topbar.addAttribute(SizeAttribute.createPercentage("width", 100));
			topbar.addAttribute(SizeAttribute.createPercentage("height", 10));
			
			// Layout
			topbar.addAttribute(LayoutAttribute.flowLayout());
			topbar.addAttribute(StringAttribute.create("direction", "horizontal"));
			topbar.addAttribute(CompassAligmentAttribute.center("alignment"));
			topbar.addAttribute(CompassAligmentAttribute.center("alignment-lr"));
			
			for(int i = 0; i < 15; i++)
			{
				RectangleNode node = new RectangleNode(topbar);
				node.setPreferredBounds(new RectangleF(32, 32));
				
				node.addAttribute(SizeAttribute.createUnit("padding-x", 2));
				node.addAttribute(SizeAttribute.createUnit("padding-y", 2));
			}
		}
		
		// Center View
		{
			ContainerNode centerview = new ContainerNode(root);
			
			// position
			centerview.addAttribute(CompassAligmentAttribute.south("vertical-align"));
			centerview.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
			
			// size
			centerview.addAttribute(SizeAttribute.createPercentage("width", 100));
			centerview.addAttribute(SizeAttribute.createPercentage("height", 90));
			
			// Layout
			centerview.addAttribute(LayoutAttribute.splitpaneLayout());
			centerview.addAttribute(StringAttribute.create("direction", "vertical"));
			centerview.addAttribute(SizeAttribute.createPercentage("splitpos", 20));
			
			{
				ContainerNode leftpane = new ContainerNode(centerview);
				
				leftpane.addAttribute(LayoutAttribute.gridLayout());
				leftpane.addAttribute(NumberAttribute.createUnit("cells-x", 1));
				leftpane.addAttribute(NumberAttribute.createUnit("cells-y", 16));
				
				for(int i = 0; i < 16; i++)
				{
					RectangleNode node = new RectangleNode(leftpane);
					node.addAttribute(SizeAttribute.createPercentage("width", 100));
					node.addAttribute(SizeAttribute.createUnit("padding-x", 2));
					node.addAttribute(SizeAttribute.createUnit("padding-y", 2));
				}
			}
			
			
			
			{
				ContainerNode rightpane = new ContainerNode(centerview);
				rightpane.addAttribute(LayoutAttribute.compassLayout());
				
				{
					RectangleNode node = new RectangleNode(rightpane);
					node.addAttribute(SizeAttribute.createPercentage("width", 75));
					node.addAttribute(SizeAttribute.createPercentage("height", 5));
					node.addAttribute(CompassAligmentAttribute.north("vertical-align"));
					node.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
				}
				
				{
					RectangleNode node = new RectangleNode(rightpane);
					node.addAttribute(SizeAttribute.createPercentage("width", 50));
					node.addAttribute(SizeAttribute.createPercentage("height", 5));
					node.addAttribute(CompassAligmentAttribute.south("vertical-align"));
					node.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
				}
				
				{
					ContainerNode grid = new ContainerNode(rightpane);
					grid.addAttribute(SizeAttribute.createUnit("width", 384));
					grid.addAttribute(SizeAttribute.createUnit("height", 384));
					grid.addAttribute(CompassAligmentAttribute.center("vertical-align"));
					grid.addAttribute(CompassAligmentAttribute.center("horizontal-align"));
					
					int cellsX = 16;
					int cellsY = 16;
					grid.addAttribute(LayoutAttribute.gridLayout());
					grid.addAttribute(NumberAttribute.createUnit("cells-x", cellsX));
					grid.addAttribute(NumberAttribute.createUnit("cells-y", cellsY));
					
					grid.addAttribute(SizeAttribute.createUnit("cell-padding-x", 2));
					grid.addAttribute(SizeAttribute.createUnit("cell-padding-y", 2));
					
					int cellsI = cellsX * cellsY;
					
					for(int i = 0; i < cellsI; i++)
					{
						new RectangleNode(grid);
					}
				}
				
			}// 'rightpane' END
			
		}// 'centerview' END
		
	}// method END
	
	private void buildDebugImage(int SCR_WIDTH, int SCR_HEIGHT, ContainerNode root, String resultFileName)
	{
		if(faceImage == null)
		{
			BufferedImage faceImage = new BufferedImage(2,2,BufferedImage.TYPE_INT_ARGB);
			int CA = 0x9FFF7FFF;
			int CB = 0x9F777777;
			
			faceImage.setRGB(0, 0, CA);
			faceImage.setRGB(1, 1, CA);
			faceImage.setRGB(1, 0, CB);
			faceImage.setRGB(0, 1, CB);
			TestRangeA.faceImage = faceImage;
		}
		
		BufferedImage finalImage = new BufferedImage(SCR_WIDTH,SCR_HEIGHT,BufferedImage.TYPE_INT_ARGB);
		Graphics g = finalImage.createGraphics();
		{
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, SCR_WIDTH, SCR_HEIGHT);
			g.setFont(g.getFont().deriveFont(14F));
			this.drawNode(root, g);
		}
		g.dispose();
		
		
		File out = new File(resultFileName);
		try
		{
			ImageIO.write(finalImage, "PNG", out);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		
	}
	
	private void drawNode(RectangleNode node, Graphics g)
	{
		RectangleF bounds = node.getBounds();
		
		if(node.getParent() != null)
		{
			g.drawImage(faceImage, (int)bounds.x, (int)bounds.y, (int)bounds.width, (int)bounds.height, null);
		}
		
		if((bounds.width*bounds.height) >= (6*6))
		{
			g.setColor(Color.BLACK);
			g.drawRect((int)bounds.x, (int)bounds.y, (int)bounds.width-1, (int)bounds.height-1);
			
			g.setColor(Color.GRAY);
			g.drawLine((int)bounds.x+1, (int)bounds.y+1, (int)((bounds.x + bounds.width)-2), (int)((bounds.y + bounds.height)-2));
			g.drawLine((int)bounds.x+1, (int)((bounds.y + bounds.height)-2), (int)((bounds.x + bounds.width)-2), (int)bounds.y+1);
		}
		
		if(node instanceof ContainerNode)
		{
			ArrayList<RectangleNode> childs = ((ContainerNode)node).getChildrens();
			
			Graphics g2 = g.create();
			{
				g2.translate((int)bounds.x, (int)bounds.y);
				
				for(RectangleNode child : childs)
				{
					this.drawNode(child, g2);
				}
			}
			g2.dispose();
		}
		
	}

}
