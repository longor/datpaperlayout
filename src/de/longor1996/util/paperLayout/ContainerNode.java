package de.longor1996.util.paperLayout;

import java.util.ArrayList;

public class ContainerNode extends RectangleNode
{
	protected ArrayList<RectangleNode> childrens;
	
	public ContainerNode(ContainerNode parentNode)
	{
		super(parentNode);
		this.childrens = new ArrayList<RectangleNode>();
	}
	
	public ArrayList<RectangleNode> getChildrens()
	{
		return this.childrens;
	}
	
	public void addChild(RectangleNode node)
	{
		this.childrens.add(node);
	}
	
}
