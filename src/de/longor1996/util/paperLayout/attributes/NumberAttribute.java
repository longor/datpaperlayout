package de.longor1996.util.paperLayout.attributes;

import de.longor1996.util.paperLayout.Attribute;
import de.longor1996.util.paperLayout.ContainerNode;

public class NumberAttribute extends Attribute
{
	final int value;
	
	public NumberAttribute(String name, int VALUE)
	{
		super(name);
		this.value = VALUE;
	}
	
	public static final NumberAttribute createUnit(String name, int value)
	{
		return new NumberAttribute(name, value);
	}
	
	public static NumberAttribute castOrNull(Attribute attribute)
	{
		if(attribute == null)
		{
			return null;
		}
		
		if(attribute instanceof NumberAttribute)
		{
			return (NumberAttribute) attribute;
		}
		
		return null;
	}

	public int getValue() {
		return this.value;
	}

	public static int getAndCastAndNull(ContainerNode container, String string, int i)
	{
		NumberAttribute attr = castOrNull(container.getAttribute(string));
		
		if(attr != null)
		{
			return attr.value;
		}
		
		return i;
	}
	
}
