package de.longor1996.util.paperLayout.attributes;

import de.longor1996.util.paperLayout.Attribute;

public class SizeAttribute extends Attribute
{
	final float value;
	final boolean isPercentage;
	
	public SizeAttribute(String name, float VALUE, boolean isPercentage)
	{
		super(name);
		this.value = VALUE;
		this.isPercentage = isPercentage;
	}
	
	public static final SizeAttribute createUnit(String name, float value)
	{
		return new SizeAttribute(name, value, false);
	}
	
	public static final SizeAttribute createPercentage(String name, float value)
	{
		return new SizeAttribute(name, value, true);
	}
	
	public static SizeAttribute castOrNull(Attribute attribute)
	{
		if(attribute == null)
		{
			return null;
		}
		
		if(attribute instanceof SizeAttribute)
		{
			return (SizeAttribute) attribute;
		}
		
		return null;
	}
	
	public float calculate(float parent)
	{
		if(this.isPercentage)
		{
			return parent * (this.value/100F);
		}
		
		return this.value;
	}
	
}
