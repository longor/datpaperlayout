package de.longor1996.util.paperLayout.attributes;

import de.longor1996.util.paperLayout.Attribute;

public class StringAttribute extends Attribute
{
	final String value;
	
	public StringAttribute(String name, String VALUE)
	{
		super(name);
		this.value = VALUE;
	}
	
	public static final StringAttribute create(String name, String value)
	{
		return new StringAttribute(name, value);
	}
	
	public static StringAttribute castOrNull(Attribute attribute)
	{
		if(attribute == null)
		{
			return null;
		}
		
		if(attribute instanceof StringAttribute)
		{
			return (StringAttribute) attribute;
		}
		
		return null;
	}
	
	public String getValue()
	{
		return this.value;
	}
	
}
