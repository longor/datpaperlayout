package de.longor1996.util.paperLayout.attributes;

import de.longor1996.util.paperLayout.Attribute;

public class PositionAttribute extends Attribute
{
	final float value;
	final boolean isPercentage;
	
	public PositionAttribute(String name, float VALUE, boolean isPercentage)
	{
		super(name);
		this.value = VALUE;
		this.isPercentage = isPercentage;
	}
	
	public static final PositionAttribute createUnit(String name, float value)
	{
		return new PositionAttribute(name, value, false);
	}
	
	public static final PositionAttribute createPercentage(String name, float value)
	{
		return new PositionAttribute(name, value, false);
	}
	
	public static PositionAttribute castOrNull(Attribute attribute)
	{
		if(attribute == null)
		{
			return null;
		}
		
		if(attribute instanceof PositionAttribute)
		{
			return (PositionAttribute) attribute;
		}
		
		return null;
	}
	
	public float calculate(float parent)
	{
		if(this.isPercentage)
		{
			return parent * (this.value/100F);
		}
		
		return this.value;
	}
	
}
