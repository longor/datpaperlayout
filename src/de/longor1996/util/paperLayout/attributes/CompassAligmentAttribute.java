package de.longor1996.util.paperLayout.attributes;

import de.longor1996.util.paperLayout.Attribute;

public class CompassAligmentAttribute extends Attribute
{
	public static final int NORTH = 1;
	public static final int WEST = 1;
	public static final int TOP = 1;
	public static final int LEFT = 1;
	
	public static final int SOUTH = 2;
	public static final int EAST = 2;
	public static final int BOTTOM = 2;
	public static final int RIGHT = 2;
	
	public static final int CENTER = 3;
	
	public final int align;
	
	public CompassAligmentAttribute(String name, int align)
	{
		super(name);
		this.align = align;
	}
	
	public static CompassAligmentAttribute center(String name)
	{
		return new CompassAligmentAttribute(name, CENTER);
	}
	
	public static Attribute north(String name)
	{
		return new CompassAligmentAttribute(name, NORTH);
	}
	
	public static Attribute south(String name)
	{
		return new CompassAligmentAttribute(name, SOUTH);
	}
	
	public static Attribute west(String name)
	{
		return new CompassAligmentAttribute(name, WEST);
	}
	
	public static Attribute east(String name)
	{
		return new CompassAligmentAttribute(name, EAST);
	}

	public static CompassAligmentAttribute castOrNull(Attribute attribute)
	{
		if(attribute == null)
		{
			return null;
		}
		
		if(attribute instanceof CompassAligmentAttribute)
		{
			return (CompassAligmentAttribute) attribute;
		}
		
		return null;
	}
	
}
