package de.longor1996.util.paperLayout.attributes;

import de.longor1996.util.paperLayout.Attribute;

// TODO: Make all xxxLayout methods return singleton instances to consume less memory.
public class LayoutAttribute extends Attribute
{
	final String layoutManagerName;
	
	public LayoutAttribute(String name, String layout)
	{
		super(name);
		this.layoutManagerName = layout;
	}
	
	public String getLayoutManagerName()
	{
		return this.layoutManagerName;
	}
	
	public static LayoutAttribute nullLayout()
	{
		return new LayoutAttribute("layout", "null");
	}
	
	public static LayoutAttribute compassLayout()
	{
		return new LayoutAttribute("layout", "compass");
	}
	
	public static LayoutAttribute gridLayout()
	{
		return new LayoutAttribute("layout", "grid");
	}
	
	public static LayoutAttribute flowLayout()
	{
		return new LayoutAttribute("layout", "flow");
	}
	
	public static LayoutAttribute splitpaneLayout()
	{
		return new LayoutAttribute("layout", "splitpane");
	}

	public static LayoutAttribute castOrNull(Attribute attribute)
	{
		if(attribute == null)
		{
			return null;
		}
		
		if(attribute instanceof LayoutAttribute)
		{
			return (LayoutAttribute) attribute;
		}
		
		return null;
	}
	
}
