package de.longor1996.util.paperLayout;

public abstract class Attribute
{
	protected final String name;
	
	public Attribute(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
	
}
