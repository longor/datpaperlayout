package de.longor1996.util.paperLayout;

import java.util.HashMap;

import de.longor1996.util.math.geom2D.RectangleF;
import de.longor1996.util.paperLayout.attributes.StringAttribute;

public class RectangleNode
{
	protected ContainerNode parentNode;
	protected RectangleF preferredBounds;
	protected RectangleF intermediateBounds;
	protected RectangleF finalizedBounds;
	protected final HashMap<String, Attribute> attributes;
	
	private RectangleNode()
	{
		this.attributes = new HashMap<String, Attribute>(4);
	}
	
	public RectangleNode(ContainerNode parentNode)
	{
		this();
		this.parentNode = parentNode;
		this.preferredBounds = new RectangleF(0, 0, 0, 0);
		this.intermediateBounds = new RectangleF(0, 0, 0, 0);
		this.finalizedBounds = new RectangleF(0, 0, 0, 0);
		
		if(parentNode != null)
		{
			this.parentNode.addChild(this);
		}
	}
	
	public void setPreferredBounds(RectangleF rect)
	{
		this.preferredBounds = rect;
	}
	
	public RectangleF getPreferredBounds()
	{
		return this.preferredBounds;
	}
	
	public RectangleF getBounds()
	{
		return this.finalizedBounds;
	}
	
	public void addAttribute(Attribute attribute)
	{
		this.attributes.put(attribute.name, attribute);
	}
	
	public Attribute getAttribute(String name)
	{
		return this.attributes.get(name);
	}
	
	public HashMap<String, Attribute> getAttributes()
	{
		return this.attributes;
	}
	
	@Override
	public String toString()
	{
		StringAttribute name = StringAttribute.castOrNull(this.attributes.get("id"));
		
		if(name != null)
		{
			return
			"RectangleNode:{name:"+((StringAttribute)name).getValue()+",bounds:["+this.preferredBounds+","+this.intermediateBounds+","+this.finalizedBounds+"]}";
		}
		else
		{
			return
			"RectangleNode:{hashCode:"+super.hashCode()+",bounds:["+this.preferredBounds+","+this.intermediateBounds+","+this.finalizedBounds+"]}";
		}
	}

	public ContainerNode getParent()
	{
		return this.parentNode;
	}
	
}
