package de.longor1996.util.paperLayout;

import de.longor1996.util.math.geom2D.RectangleF;
import de.longor1996.util.paperLayout.attributes.CompassAligmentAttribute;
import de.longor1996.util.paperLayout.attributes.LayoutAttribute;
import de.longor1996.util.paperLayout.attributes.NumberAttribute;
import de.longor1996.util.paperLayout.attributes.PositionAttribute;
import de.longor1996.util.paperLayout.attributes.SizeAttribute;

public class PaperInterpeter
{
	private ContainerNode rootNode;
	private boolean roundEverything;
	private boolean debug = false;

	public PaperInterpeter()
	{
		this.rootNode = new ContainerNode(null);
		this.roundEverything = true;
		
		// The debug-flag doesn't do much, yet.
		this.debug = false;
	}
	
	public void setDebug(boolean flag)
	{
		this.debug = flag;
	}
	
	public void setRoundEverything(boolean round)
	{
		this.roundEverything = round;
	}
	
	public ContainerNode getRoot()
	{
		return this.rootNode;
	}
	
	public void doLayout(float rootWidth, float rootHeight)
	{
		
		// Setup 'screen' rectangle.
		RectangleF rootRect = new RectangleF(rootWidth, rootHeight);
		this.rootNode.setPreferredBounds(rootRect);
		this.rootNode.intermediateBounds = rootRect;
		this.rootNode.finalizedBounds = rootRect;
		
		// start time
		long start = System.nanoTime();
		
		// Recalculate Bounds
		this.recalculateDimensions(this.rootNode);
		
		// Lay-out ALL the rectangles!
		this.layoutContents(this.rootNode);
		
		if(this.debug)
		{
			System.out.println("Done building Layout. Took " + (System.nanoTime() - start) + "ns/"+((System.nanoTime() - start)/1000000)+"ms!");
		}
	}
	
	private void recalculateDimensions(ContainerNode container)
	{
		for(RectangleNode child : container.childrens)
		{
			SizeAttribute $attributeWidth = SizeAttribute.castOrNull(child.attributes.get("width"));
			SizeAttribute $attributeHeight = SizeAttribute.castOrNull(child.attributes.get("height"));
			float width = child.preferredBounds.width;
			float height = child.preferredBounds.height;
			boolean changed = false;
			
			if($attributeWidth != null)
			{
				width = $attributeWidth.calculate(container.intermediateBounds.width);
				changed = true;
			}
			
			if($attributeHeight != null)
			{
				height = $attributeHeight.calculate(container.intermediateBounds.height);
				changed = true;
			}
			
			if(changed)
			{
				child.intermediateBounds = new RectangleF(child.preferredBounds.x, child.preferredBounds.y, width, height);
			}
			else
			{
				child.intermediateBounds = child.preferredBounds;
			}
			
			if(child instanceof ContainerNode)
			{
				this.recalculateDimensions((ContainerNode) child);
			}
		}
	}
	
	private void layoutContents(ContainerNode container)
	{
		Attribute layoutAttribute = container.attributes.get("layout");
		
		if(layoutAttribute == null)
		{
			System.out.println("[Error] No Attribute assigned to 'layout'-name.");
			layoutAttribute = LayoutAttribute.nullLayout();
		}
		
		if(!(layoutAttribute instanceof LayoutAttribute))
		{
			System.out.println("[Error] Wrong Attribute Type assigned to 'layout'-name.");
			layoutAttribute = LayoutAttribute.nullLayout();
		}
		
		String layoutManager = ((LayoutAttribute)layoutAttribute).getLayoutManagerName();
		
		if("null".equals(layoutManager))
		{
			this.layout_null(container);
		}
		
		if("compass".equals(layoutManager))
		{
			this.layout_compass(container);
		}
		
		if("grid".equals(layoutManager))
		{
			this.layout_grid(container);
		}
		
	}
	
	private void layout_grid(ContainerNode container)
	{
		NumberAttribute $attributeCellsX = NumberAttribute.castOrNull(container.attributes.get("cells-x"));
		NumberAttribute $attributeCellsY = NumberAttribute.castOrNull(container.attributes.get("cells-y"));
		
		if(($attributeCellsX == null) || ($attributeCellsY == null))
		{
			for(RectangleNode child : container.childrens)
			{
				child.finalizedBounds = child.preferredBounds;
			}
		}
		
		RectangleF containerBounds = container.intermediateBounds;
		
		int cellsX = $attributeCellsX.getValue();
		int cellsY = $attributeCellsY.getValue();
		
		int cellPaddingX = NumberAttribute.getAndCastAndNull(container, "cell-padding-x", 0);
		int cellPaddingY = NumberAttribute.getAndCastAndNull(container, "cell-padding-y", 0);
		
		float cellWidth = this.round(containerBounds.width / (float)cellsX);
		float cellHeight = this.round(containerBounds.height / (float)cellsY);
		
		if((containerBounds.width > 4) || (containerBounds.height > 4))
		{
			cellWidth = (float) Math.round(cellWidth);
			cellHeight = (float) Math.round(cellHeight);
		}
		
		outerLoop:
		for(int y = 0, childIndex = 0; y < cellsY; y++)
		{
			for(int x = 0; x < cellsX; x++, childIndex++)
			{
				if(childIndex > (container.childrens.size()-1))
				{
					break outerLoop;
				}
				
				RectangleNode child = container.childrens.get(childIndex);
				
				float bx = this.round(x * cellWidth);
				float by = this.round(y * cellHeight);
				float bw = cellWidth;
				float bh = cellHeight;
				
				if(cellPaddingX > 0) // X-Axis Padding.
				{
					bx += cellPaddingX;
					bw -= cellPaddingX * 2;
				}
				
				if(cellPaddingY > 0) // X-Axis Padding.
				{
					by += cellPaddingY;
					bh -= cellPaddingY * 2;
				}
				
				if((bx+bw) > containerBounds.width)
				{
					bw -= (bx+bw) - containerBounds.width;
				}
				if((by+bh) > containerBounds.height)
				{
					bh -= (by+bh) - containerBounds.height;
				}
				
				child.finalizedBounds = child.intermediateBounds = new RectangleF(this.round(bx), this.round(by), this.round(bw), this.round(bh));
				
				if(child instanceof ContainerNode)
				{
					this.layoutContents((ContainerNode) child);
				}
			}
		}
		
	}

	private void layout_compass(ContainerNode container)
	{
		for(RectangleNode child : container.childrens)
		{
			RectangleF size = child.intermediateBounds;
			CompassAligmentAttribute $aligmentHorizontal = CompassAligmentAttribute.castOrNull(child.attributes.get("horizontal-align"));
			CompassAligmentAttribute $aligmentVertical = CompassAligmentAttribute.castOrNull(child.attributes.get("vertical-align"));
			
			int aligmentHorizontal = $aligmentHorizontal == null ? 0 : $aligmentHorizontal.align;
			int aligmentVertical = $aligmentVertical == null ? 0 : $aligmentVertical.align;
			
			float finalPositionX = 0;
			float finalPositionY = 0;
			
			switch(aligmentHorizontal)
			{
				default:
				{
					finalPositionX = 0;
				}
				break;
				case CompassAligmentAttribute.CENTER:
				{
					finalPositionX = this.round(container.intermediateBounds.width / 2F) - this.round(size.width / 2F);
				}
				break;
				case CompassAligmentAttribute.WEST:
				{
					finalPositionX = 0;
				}
				break;
				case CompassAligmentAttribute.EAST:
				{
					finalPositionX = this.round(container.intermediateBounds.width - size.width);
				}
				break;
			}
			
			switch(aligmentVertical)
			{
				default:
				{
					finalPositionY = 0;
				}
				break;
				case CompassAligmentAttribute.CENTER:
				{
					finalPositionY = this.round(container.intermediateBounds.height / 2F) - this.round(size.height / 2F);
				}
				break;
				case CompassAligmentAttribute.NORTH:
				{
					finalPositionY = 0;
				}
				break;
				case CompassAligmentAttribute.SOUTH:
				{
					finalPositionY = this.round(container.intermediateBounds.height - size.height);
				}
				break;
			}
			
			PositionAttribute $offsetHorizontal = PositionAttribute.castOrNull(child.attributes.get("offset-x"));
			PositionAttribute $offsetVertical = PositionAttribute.castOrNull(child.attributes.get("offset-y"));
			
			if($offsetHorizontal != null)
			{
				finalPositionX += $offsetHorizontal.calculate(container.intermediateBounds.width);
			}
			if($offsetVertical != null)
			{
				finalPositionY += $offsetVertical.calculate(container.intermediateBounds.height);
			}
			
			child.finalizedBounds = new RectangleF(
											this.round(finalPositionX),
											this.round(finalPositionY),
											this.round(size.width),
											this.round(size.height)
			);
			
			if(child instanceof ContainerNode)
			{
				this.layoutContents((ContainerNode) child);
			}
		}
	}
	
	private void layout_null(ContainerNode container)
	{
		for(RectangleNode child : container.childrens)
		{
			child.finalizedBounds = child.intermediateBounds;
			
			if(child instanceof ContainerNode)
			{
				this.layoutContents((ContainerNode) child);
			}
		}
	}
	
	private float round(float in)
	{
		return this.roundEverything ? Math.round(in) : in;
	}
	
}
