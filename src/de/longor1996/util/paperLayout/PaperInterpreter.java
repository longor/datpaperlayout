package de.longor1996.util.paperLayout;

import de.longor1996.util.math.geom2D.RectangleF;
import de.longor1996.util.paperLayout.attributes.CompassAligmentAttribute;
import de.longor1996.util.paperLayout.attributes.LayoutAttribute;
import de.longor1996.util.paperLayout.attributes.NumberAttribute;
import de.longor1996.util.paperLayout.attributes.PositionAttribute;
import de.longor1996.util.paperLayout.attributes.SizeAttribute;
import de.longor1996.util.paperLayout.attributes.StringAttribute;

public class PaperInterpreter
{
	private ContainerNode rootNode;
	private boolean roundEverything;
	private boolean debug = false;

	public PaperInterpreter()
	{
		this.rootNode = new ContainerNode(null);
		this.roundEverything = true;
		
		// The debug-flag doesn't do much, yet.
		this.debug = false;
	}
	
	public void setDebug(boolean flag)
	{
		this.debug = flag;
	}
	
	public void setRoundEverything(boolean round)
	{
		this.roundEverything = round;
	}
	
	public ContainerNode getRoot()
	{
		return this.rootNode;
	}
	
	public void doLayout(float rootWidth, float rootHeight)
	{
		
		// Setup 'screen' rectangle.
		RectangleF rootRect = new RectangleF(rootWidth, rootHeight);
		this.rootNode.setPreferredBounds(rootRect);
		this.rootNode.intermediateBounds = rootRect;
		this.rootNode.finalizedBounds = rootRect;
		
		// start time
		long start = System.nanoTime();
		
		// Lay-out ALL the rectangles!
		this.layoutContents(this.rootNode);
		
		if(this.debug)
		{
			System.out.println("Done building Layout. Took " + (System.nanoTime() - start) + "ns/"+((System.nanoTime() - start)/1000000)+"ms!");
		}
	}
	
	private void recalculateDimensions(ContainerNode container)
	{
		for(RectangleNode child : container.childrens)
		{
			SizeAttribute $attributeWidth = SizeAttribute.castOrNull(child.attributes.get("width"));
			SizeAttribute $attributeHeight = SizeAttribute.castOrNull(child.attributes.get("height"));
			float width = child.preferredBounds.width;
			float height = child.preferredBounds.height;
			boolean changed = false;
			
			if($attributeWidth != null)
			{
				width = $attributeWidth.calculate(container.intermediateBounds.width);
				changed = true;
			}
			
			if($attributeHeight != null)
			{
				height = $attributeHeight.calculate(container.intermediateBounds.height);
				changed = true;
			}
			
			if(changed)
			{
				child.intermediateBounds = new RectangleF(child.preferredBounds.x, child.preferredBounds.y, width, height);
			}
			else
			{
				child.intermediateBounds = child.preferredBounds;
			}
			
			if(child instanceof ContainerNode)
			{
				this.recalculateDimensions((ContainerNode) child);
			}
		}
	}
	
	private void layoutContents(ContainerNode container)
	{
		// Recalculate Bounds
		this.recalculateDimensions(container);
		
		Attribute layoutAttribute = container.attributes.get("layout");
		
		if(layoutAttribute == null)
		{
			System.out.println("[Error] No Attribute assigned to 'layout'-name.");
			layoutAttribute = LayoutAttribute.nullLayout();
		}
		
		if(!(layoutAttribute instanceof LayoutAttribute))
		{
			System.out.println("[Error] Wrong Attribute Type assigned to 'layout'-name.");
			layoutAttribute = LayoutAttribute.nullLayout();
		}
		
		String layoutManager = ((LayoutAttribute)layoutAttribute).getLayoutManagerName();
		
		if("null".equals(layoutManager))
		{
			this.layout_null(container);
		}
		
		if("compass".equals(layoutManager))
		{
			this.layout_compass(container);
		}
		
		if("grid".equals(layoutManager))
		{
			this.layout_grid(container);
		}
		
		if("flow".equals(layoutManager))
		{
			this.layout_flow(container);
		}
		
		if("splitpane".equals(layoutManager))
		{
			this.layout_splitpane(container);
		}
		
	}
	
	private void layout_splitpane(ContainerNode container)
	{
		if(container.childrens.size() != 2)
		{
			System.err.println("Wrong number of components in splitpane. Must be exactly two.");
			return;
		}
		
		StringAttribute $direction = StringAttribute.castOrNull(container.attributes.get("direction"));
		int direction = 1;
		
		if($direction == null)
		{
			direction = 1;
		}
		
		if("horizontal".equals($direction.getValue()))
		{
			direction = 0;
		}
		
		if("vertical".equals($direction.getValue()))
		{
			direction = 1;
		}
		
		SizeAttribute $splitPos = SizeAttribute.castOrNull(container.attributes.get("splitpos"));
		
		
		RectangleNode A = container.childrens.get(0);
		RectangleNode B = container.childrens.get(1);
		
		float Ax = 0;// Always zero
		float Ay = 0;// Always zero
		float Aw = 0;
		float Ah = 0;
		
		float Bx = 0;
		float By = 0;
		float Bw = 0;
		float Bh = 0;
		
		switch(direction)
		{
			case 0:
			{
				float splitPos = $splitPos == null ? (container.intermediateBounds.height/2) : $splitPos.calculate(container.intermediateBounds.height);
				splitPos = this.round(splitPos);
				
				Ax = 0;
				Ay = 0;
				Aw = container.intermediateBounds.width;
				Ah = splitPos;
				
				Bx = 0;
				By = splitPos;
				Bw = container.intermediateBounds.width;
				Bh = container.intermediateBounds.height - splitPos;
			}
			break;
			case 1:
			{
				float splitPos = $splitPos == null ? (container.intermediateBounds.width/2) : $splitPos.calculate(container.intermediateBounds.width);
				splitPos = this.round(splitPos);
				
				Ax = 0;
				Ay = 0;
				Aw = splitPos;
				Ah = container.intermediateBounds.height;
				
				Bx = splitPos;
				By = 0;
				Bw = container.intermediateBounds.width - splitPos;
				Bh = container.intermediateBounds.height;
			}
			break;
			default: throw new IllegalStateException("Unknown direction for ["+container+"] in layout_splitpane: " + direction);
		}
		
		A.intermediateBounds = new RectangleF(Ax,Ay,Aw,Ah);
		this.padding(A);
		A.finalizedBounds = A.intermediateBounds;
		
		B.intermediateBounds = new RectangleF(Bx,By,Bw,Bh);
		this.padding(B);
		B.finalizedBounds = B.intermediateBounds;
		
		if(A instanceof ContainerNode)
		{
			this.layoutContents((ContainerNode) A);
		}
		if(B instanceof ContainerNode)
		{
			this.layoutContents((ContainerNode) B);
		}
		
	}
	
	private void layout_flow(ContainerNode container)
	{
		RectangleF containerRect = container.intermediateBounds;
		
		// Alignment: 0 Horizontal, 1 Vertical
		StringAttribute $direction = StringAttribute.castOrNull(container.attributes.get("direction"));
		int direction = 0;
		
		if($direction == null)
		{
			direction = 0;
		}
		
		if("horizontal".equals($direction.getValue()))
		{
			direction = 0;
		}
		
		if("vertical".equals($direction.getValue()))
		{
			direction = 1;
		}
		
		CompassAligmentAttribute $alignment = CompassAligmentAttribute.castOrNull(container.attributes.get("alignment"));
		int alignment = $alignment == null ? CompassAligmentAttribute.CENTER : $alignment.align;
		
		float offX = 0;
		float offY = 0;
		
		// TODO: Add a way to make the FlowLayout 'center' its nodes.
		// Such as that the offX/offY don't start at a value of F,
		// but rather at F = ((container.width|height / 2) - (combinedNodeSize.Width|Height / 2))
		
		float combinedNodeWidth = 0;
		float combinedNodeHeight = 0;
		{
			for(RectangleNode child : container.childrens)
			{
				combinedNodeWidth += child.intermediateBounds.width;
				combinedNodeHeight += child.intermediateBounds.height;
			}
		}
		
		CompassAligmentAttribute $alignment_lr = CompassAligmentAttribute.castOrNull(container.attributes.get("alignment-lr"));
		int alignment_lr = $alignment_lr == null ? CompassAligmentAttribute.CENTER : $alignment_lr.align;
		
		if(direction == 0)
		{
			// Horizontal LR Alignment
			switch(alignment_lr)
			{
			case CompassAligmentAttribute.CENTER: offX = (containerRect.width/2) - (combinedNodeWidth/2); break;
			case CompassAligmentAttribute.EAST: offX = containerRect.width - combinedNodeWidth; break;
			case CompassAligmentAttribute.WEST: offX = 0; break;
			default: throw new IllegalStateException("Unknown alignment-lr in layout_flow: " + direction);
			}
		}
		else if(direction == 1)
		{
			// Horizontal LR Alignment
			switch(alignment_lr)
			{
			case CompassAligmentAttribute.CENTER: offY = (containerRect.height/2) - (combinedNodeHeight/2); break;
			case CompassAligmentAttribute.SOUTH: offY = containerRect.height - combinedNodeHeight; break;
			case CompassAligmentAttribute.NORTH: offY = 0; break;
			default: throw new IllegalStateException("Unknown alignment-lr in layout_flow: " + direction);
			}
		}
		
		
		for(RectangleNode child : container.childrens)
		{
			RectangleF preBounds = child.intermediateBounds;
			float bx = preBounds.x;
			float by = preBounds.y;
			float bw = preBounds.width;
			float bh = preBounds.height;
			
			bx += offX;
			by += offY;
			
			if((direction == 1) && (bw < container.intermediateBounds.width))
			{
				switch(alignment)
				{
					case CompassAligmentAttribute.CENTER:
					{
						bx = this.round(container.intermediateBounds.width / 2F) - this.round(preBounds.width / 2F);
					}
					break;
					case CompassAligmentAttribute.WEST:
					{
						bx = 0;
					}
					break;
					case CompassAligmentAttribute.EAST:
					{
						bx = this.round(container.intermediateBounds.width - preBounds.width);
					}
					break;
					default: throw new IllegalStateException("Unknown alignment for ["+child+"] in layout_flow: " + direction);
				}
			}
			
			if((direction == 0) && (bh < container.intermediateBounds.height))
			{
				switch(alignment)
				{
					case CompassAligmentAttribute.CENTER:
					{
						by = this.round(container.intermediateBounds.height / 2F) - this.round(preBounds.height / 2F);
					}
					break;
					case CompassAligmentAttribute.NORTH:
					{
						by = 0;
					}
					break;
					case CompassAligmentAttribute.SOUTH:
					{
						by = this.round(container.intermediateBounds.height - preBounds.height);
					}
					break;
					default: throw new IllegalStateException("Unknown alignment for ["+child+"] in layout_flow: " + direction);
				}
			}
			
			child.intermediateBounds = new RectangleF(this.round(bx), this.round(by), this.round(bw), this.round(bh));
			this.padding(child);
			child.finalizedBounds = child.intermediateBounds;
			
			switch(direction)
			{
			case 0: offX += bw; break;
			case 1: offY += bh; break;
			default: throw new IllegalStateException("Unknown direction in layout_flow: " + direction + ". How did this happen?!");
			}
			
			if(child instanceof ContainerNode)
			{
				this.layoutContents((ContainerNode) child);
			}
			
		}
		
		
		
		
		
	}

	private void layout_grid(ContainerNode container)
	{
		NumberAttribute $attributeCellsX = NumberAttribute.castOrNull(container.attributes.get("cells-x"));
		NumberAttribute $attributeCellsY = NumberAttribute.castOrNull(container.attributes.get("cells-y"));
		
		if(($attributeCellsX == null) || ($attributeCellsY == null))
		{
			for(RectangleNode child : container.childrens)
			{
				child.finalizedBounds = child.preferredBounds;
			}
		}
		
		RectangleF containerBounds = container.intermediateBounds;
		
		int cellsX = $attributeCellsX.getValue();
		int cellsY = $attributeCellsY.getValue();
		
		int cellPaddingX = NumberAttribute.getAndCastAndNull(container, "cell-padding-x", 0);
		int cellPaddingY = NumberAttribute.getAndCastAndNull(container, "cell-padding-y", 0);
		
		float cellWidth = this.round(containerBounds.width / (float)cellsX);
		float cellHeight = this.round(containerBounds.height / (float)cellsY);
		
		if((containerBounds.width > 4) || (containerBounds.height > 4))
		{
			cellWidth = (float) Math.round(cellWidth);
			cellHeight = (float) Math.round(cellHeight);
		}
		
		outerLoop:
		for(int y = 0, childIndex = 0; y < cellsY; y++)
		{
			for(int x = 0; x < cellsX; x++, childIndex++)
			{
				if(childIndex > (container.childrens.size()-1))
				{
					break outerLoop;
				}
				
				RectangleNode child = container.childrens.get(childIndex);
				
				float bx = this.round(x * cellWidth);
				float by = this.round(y * cellHeight);
				float bw = cellWidth;
				float bh = cellHeight;
				
				if(cellPaddingX > 0) // X-Axis Padding.
				{
					bx += cellPaddingX;
					bw -= cellPaddingX * 2;
				}
				
				if(cellPaddingY > 0) // X-Axis Padding.
				{
					by += cellPaddingY;
					bh -= cellPaddingY * 2;
				}
				
				if((bx+bw) > containerBounds.width)
				{
					bw -= (bx+bw) - containerBounds.width;
				}
				if((by+bh) > containerBounds.height)
				{
					bh -= (by+bh) - containerBounds.height;
				}
				
				
				child.intermediateBounds = new RectangleF(this.round(bx), this.round(by), this.round(bw), this.round(bh));
				this.padding(child);
				child.finalizedBounds = child.intermediateBounds;
				
				if(child instanceof ContainerNode)
				{
					this.layoutContents((ContainerNode) child);
				}
			}
		}
		
	}

	private void layout_compass(ContainerNode container)
	{
		for(RectangleNode child : container.childrens)
		{
			RectangleF size = child.intermediateBounds;
			CompassAligmentAttribute $aligmentHorizontal = CompassAligmentAttribute.castOrNull(child.attributes.get("horizontal-align"));
			CompassAligmentAttribute $aligmentVertical = CompassAligmentAttribute.castOrNull(child.attributes.get("vertical-align"));
			
			int aligmentHorizontal = $aligmentHorizontal == null ? 0 : $aligmentHorizontal.align;
			int aligmentVertical = $aligmentVertical == null ? 0 : $aligmentVertical.align;
			
			float finalPositionX = 0;
			float finalPositionY = 0;
			
			switch(aligmentHorizontal)
			{
				default:
				{
					finalPositionX = 0;
				}
				break;
				case CompassAligmentAttribute.CENTER:
				{
					finalPositionX = this.round(container.intermediateBounds.width / 2F) - this.round(size.width / 2F);
				}
				break;
				case CompassAligmentAttribute.WEST:
				{
					finalPositionX = 0;
				}
				break;
				case CompassAligmentAttribute.EAST:
				{
					finalPositionX = this.round(container.intermediateBounds.width - size.width);
				}
				break;
			}
			
			switch(aligmentVertical)
			{
				default:
				{
					finalPositionY = 0;
				}
				break;
				case CompassAligmentAttribute.CENTER:
				{
					finalPositionY = this.round(container.intermediateBounds.height / 2F) - this.round(size.height / 2F);
				}
				break;
				case CompassAligmentAttribute.NORTH:
				{
					finalPositionY = 0;
				}
				break;
				case CompassAligmentAttribute.SOUTH:
				{
					finalPositionY = this.round(container.intermediateBounds.height - size.height);
				}
				break;
			}
			
			PositionAttribute $offsetHorizontal = PositionAttribute.castOrNull(child.attributes.get("offset-x"));
			PositionAttribute $offsetVertical = PositionAttribute.castOrNull(child.attributes.get("offset-y"));
			
			if($offsetHorizontal != null)
			{
				finalPositionX += $offsetHorizontal.calculate(container.intermediateBounds.width);
			}
			if($offsetVertical != null)
			{
				finalPositionY += $offsetVertical.calculate(container.intermediateBounds.height);
			}
			
			child.intermediateBounds = new RectangleF(
											this.round(finalPositionX),
											this.round(finalPositionY),
											this.round(size.width),
											this.round(size.height)
			);
			
			this.padding(child);
			
			child.finalizedBounds = child.intermediateBounds;
			
			if(child instanceof ContainerNode)
			{
				this.layoutContents((ContainerNode) child);
			}
		}
	}
	
	private void layout_null(ContainerNode container)
	{
		for(RectangleNode child : container.childrens)
		{
			child.intermediateBounds = child.preferredBounds;
			this.padding(child);
			child.finalizedBounds = child.intermediateBounds;
			
			if(child instanceof ContainerNode)
			{
				this.layoutContents((ContainerNode) child);
			}
		}
	}
	
	private float round(float in)
	{
		return this.roundEverything ? Math.round(in) : in;
	}
	
	private void padding(RectangleNode node)
	{
		SizeAttribute $paddingX = SizeAttribute.castOrNull(node.attributes.get("padding-x"));
		SizeAttribute $paddingY = SizeAttribute.castOrNull(node.attributes.get("padding-y"));
		
		SizeAttribute $paddingNorth = SizeAttribute.castOrNull(node.attributes.get("padding-north"));
		SizeAttribute $paddingEast = SizeAttribute.castOrNull(node.attributes.get("padding-east"));
		SizeAttribute $paddingSouth = SizeAttribute.castOrNull(node.attributes.get("padding-south"));
		SizeAttribute $paddingWest = SizeAttribute.castOrNull(node.attributes.get("padding-west"));
		
		node.intermediateBounds = this.padding2(node.intermediateBounds, $paddingNorth,$paddingEast,$paddingSouth,$paddingWest);
		
		float paddingX = 0;
		float paddingY = 0;
		
		if($paddingX != null)
		{
			paddingX = $paddingX.calculate(node.intermediateBounds.width);
		}
		if($paddingY != null)
		{
			paddingY = $paddingY.calculate(node.intermediateBounds.height);
		}
		if((paddingX <= 0) && (paddingY <= 0))
		{
			return;
		}
		
		node.intermediateBounds = this.padding(node.intermediateBounds, paddingX, paddingY);
	}
	
	private RectangleF padding2(RectangleF in,
			SizeAttribute $paddingNorth, SizeAttribute $paddingEast,
			SizeAttribute $paddingSouth, SizeAttribute $paddingWest)
	{
		float paddingNorth = $paddingNorth == null ? 0 : $paddingNorth.calculate(in.height);
		float paddingEast  = $paddingEast== null ? 0 : $paddingEast.calculate(in.width);
		float paddingSouth = $paddingSouth == null ? 0 : $paddingSouth.calculate(in.height);
		float paddingWest  = $paddingWest == null ? 0 : $paddingWest.calculate(in.width);
		
		float x = in.x;
		float y = in.y;
		float w = in.width;
		float h = in.height;
		
		float minX = x;
		float minY = y;
		float maxX = x + w;
		float maxY = y + h;
		
		minX += paddingWest;
		minY += paddingNorth;
		maxX -= paddingEast;
		maxY -= paddingSouth;
		
		x = minX;
		y = minY;
		w = maxX-minX;
		h = maxY-minY;
		
		if(w < 0)
		{
			w = 0;
		}
		if(h < 0)
		{
			h = 0;
		}
		
		return new RectangleF(x,y,w,h);
	}

	private RectangleF padding(RectangleF in, float pdx1, float pdy1)
	{
		float x = in.x;
		float y = in.y;
		float w = in.width;
		float h = in.height;
		
		float pdx2 = pdx1 * 2;
		float pdy2 = pdy1 * 2;
		
		if(w > pdx2)
		{
			w -= pdx2;
			x += pdx1;
		}
		
		if(h > pdy2)
		{
			h -= pdy2;
			y += pdy1;
		}
		
		return new RectangleF(x,y,w,h);
	}
	
}
