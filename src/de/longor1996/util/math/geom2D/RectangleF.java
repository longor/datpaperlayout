package de.longor1996.util.math.geom2D;

/**
 * Yet another Rectangle class. Only here to remove dependencies on
 * awt/lwjgl/etc
 * 
 * @author ryanm
 */
public class RectangleF implements Comparable<RectangleF>{
	/**
 * 
 */
	public final float x;
	
	/**
 * 
 */
	public final float y;
	
	/**
 * 
 */
	public final float width;
	
	/**
 * 
 */
	public final float height;
	
	public RectangleF()
	{
		this.x = 0;
		this.y = 0;
		this.width = 0;
		this.height = 0;
	}
	
	public RectangleF(float x, float y, float width, float height)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public RectangleF(RectangleF r)
	{
		this.x = r.x;
		this.y = r.y;
		this.width = r.width;
		this.height = r.height;
	}
	
	public RectangleF(float w, float h)
	{
		this.x = 0;
		this.y = 0;
		this.width = w;
		this.height = h;
	}
	
	public float getSurfaceArea()
	{
		return this.width * this.height;
	}
	
	@Override
	public String toString()
	{
		return "[ " + this.x + ", " + this.y + ", " + this.width + ", " + this.height + " ]";
	}
	
	@Override
	public int compareTo(RectangleF o)
	{
		return (int) Math.ceil(o.getSurfaceArea() - this.getSurfaceArea());
	}
	
	public RectangleF getRectangleCentered(float parentWidth, float parentHeight)
	{
		float halfWidth = this.width / 2F;
		float halfHeight = this.height / 2F;
		float halfParentWidth = parentWidth / 2F;
		float halfParentHeight = parentHeight / 2F;
		return new RectangleF(halfParentWidth - halfWidth, halfParentHeight - halfHeight, this.width, this.height);
	}
	
	public RectangleF getRectangleTranslated(float X, float Y)
	{
		return new RectangleF(this.x+X,this.y+Y,this.width,this.height);
	}
	
	public boolean isInside(float X, float Y)
	{
		if((X < this.x) || (Y < this.y) || (X > (this.x+this.width)) || (Y > (this.y+this.height)))
		{
			return false;
		}
		
		return true;
	}

	public RectangleF getShrinkedRectangle(float padding)
	{
		float bx = this.x;
		float by = this.y;
		float bw = this.width;
		float bh = this.height;
		
		bx += padding;
		bw -= padding * 2;
		
		by += padding;
		bh -= padding * 2;
		
		return new RectangleF(bx, by, bw, bh);
	}
	
}
